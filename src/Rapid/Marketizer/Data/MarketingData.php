<?php

namespace Rapid\Marketizer\Data;

class MarketingData
{
	protected $channel;
	protected $source;
	protected $campaign;
	protected $landingPage;
	protected $referer;
	protected $userAgent;
	protected $remoteAddress;
    protected $conversionPage;

	public static $socialMediaOptions  = ['facebook', 'twitter', 'instagram', 'linkedin', 'plus.google'];
	public static $searchEngineOptions = ['google', 'bing', 'yahoo', 'ask', 'aol'];

	public static $campaignOptions = [
        'utm_campaign' => 'facebook',
        'campaign'     => 'adwords',
	];

	const CHANNEL_SOCIAL  = 'social';
	const CHANNEL_ORGANIC = 'organic';
	const CHANNEL_DIRECT  = 'direct';
	const CHANNEL_PAID    = 'paid';

    /**
     * Gets the value of channel.
     *
     * @return mixed
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * Sets the value of channel.
     *
     * @param mixed $channel the channel
     *
     * @return self
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * Gets the value of source.
     *
     * @return mixed
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Sets the value of source.
     *
     * @param mixed $source the source
     *
     * @return self
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Gets the value of campaign.
     *
     * @return mixed
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * Sets the value of campaign.
     *
     * @param mixed $campaign the campaign
     *
     * @return self
     */
    public function setCampaign($campaign)
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * Gets the value of landingPage.
     *
     * @return mixed
     */
    public function getLandingPage()
    {
        return $this->landingPage;
    }

    /**
     * Sets the value of landingPage.
     *
     * @param mixed $landingPage the landing page
     *
     * @return self
     */
    public function setLandingPage($landingPage)
    {
        $this->landingPage = $landingPage;

        return $this;
    }

    /**
     * Gets the value of referer.
     *
     * @return mixed
     */
    public function getReferer()
    {
        return $this->referer;
    }

    /**
     * Sets the value of referer.
     *
     * @param mixed $referer the referer
     *
     * @return self
     */
    public function setReferer($referer)
    {
        $this->referer = $referer;

        return $this;
    }

    /**
     * Gets the value of userAgent.
     *
     * @return mixed
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * Sets the value of userAgent.
     *
     * @param mixed $userAgent the user agent
     *
     * @return self
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    /**
     * Gets the value of remoteAddress.
     *
     * @return mixed
     */
    public function getRemoteAddress()
    {
        return $this->remoteAddress;
    }

    /**
     * Sets the value of remoteAddress.
     *
     * @param mixed $remoteAddress the remote address
     *
     * @return self
     */
    public function setRemoteAddress($remoteAddress)
    {
        $this->remoteAddress = $remoteAddress;

        return $this;
    }

    public function toArray()
    {
        return get_object_vars($this);
    }

    /**
     * Gets the value of conversionPage.
     *
     * @return mixed
     */
    public function getConversionPage()
    {
        return $this->conversionPage;
    }

    /**
     * Sets the value of conversionPage.
     *
     * @param mixed $conversionPage the conversion page
     *
     * @return self
     */
    public function setConversionPage($conversionPage)
    {
        $this->conversionPage = $conversionPage;

        return $this;
    }
}
