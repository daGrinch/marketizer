<?php

namespace Rapid\Marketizer\Exception;

class UrlNotFoundException extends \Exception
{
	protected $message = "Url not found";
}