<?php

namespace Rapid\Marketizer\Exception;

class UrlMalformedException extends \Exception
{
	protected $message = "Url provided is malformed";
}