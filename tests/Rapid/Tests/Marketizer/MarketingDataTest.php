<?php

namespace Rapid\Tests\Marketizer;

use Rapid\Tests\TestCase;
use Rapid\Marketizer\Marketizer;
use Rapid\Marketizer\Data\MarketingData;

class MarketingDataTest extends TestCase
{
    public function testMarketingConstructor()
    {
        $this->assertInstanceOf('Rapid\Marketizer\Marketizer', new Marketizer());
    }

    public function testAdditionalInfo()
    {
        $landingPage    = "http://gorapid.com.au/";
        $conversionPage = "http://gorapid.com.au/finance/";
        $userAgent      = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";
        $remoteAddress  = "192.168.1.1";

        $this->marketizer = new Marketizer();
        $data             = $this->marketizer->getMarketingData($landingPage, null);

        $data->setConversionPage($conversionPage);
        $data->setUserAgent($userAgent);
        $data->setRemoteAddress($remoteAddress);

        $this->assertEquals($data->getConversionPage(), $conversionPage);
        $this->assertEquals($data->getUserAgent(), $userAgent);
        $this->assertEquals($data->getRemoteAddress(), $remoteAddress);
    }

    /**
     * @expectedException Rapid\Marketizer\Exception\UrlNotFoundException
     * @expectedExceptionMessage Url not found
     */
    public function testNullLandingPage()
    {
        $this->marketizer = new Marketizer();
        $this->marketizer->getMarketingData(null, null);
    }

    /**
     * @dataProvider channelDataProvider
     */
    public function testChannelAndSource($landingPage, $referer = null, $channel = null, $source = null, $campaign = null)
    {
        $this->marketizer = new Marketizer();
        $data             = $this->marketizer->getMarketingData($landingPage, $referer);

        $this->assertInstanceOf('Rapid\Marketizer\Data\MarketingData', $data);
        $this->assertEquals($channel, $data->getChannel());
        $this->assertEquals($source, $data->getSource());
        $this->assertEquals($landingPage, $data->getLandingPage());
        $this->assertEquals($referer, $data->getReferer());
        $this->assertEquals($campaign, $data->getCampaign());
    }

    public function channelDataProvider()
    {
        return [
            [
                'http://gorapid.com.au/?utm_campaign=CarSale',
                'http://www.hello.com',
                MarketingData::CHANNEL_PAID,
                'facebook',
                'CarSale'
            ],
            [
                'http://gorapid.com.au/?campaign=adwords',
                'http://www.hello.com',
                MarketingData::CHANNEL_PAID,
                'adwords',
                'adwords'
            ],
            [
                'https://gorapid.com.au/resources/customer-reviews/?campaign=RNA',
                null,
                MarketingData::CHANNEL_PAID,
                'adwords',
                'RNA',
            ],
            [
                'http://gorapid.com.au',
                'http://www.google.com/something/asdasd',
                MarketingData::CHANNEL_ORGANIC,
                'google'
            ],
            [
                'http://gorapid.com.au',
                'http://www.google.com.au/something/asdasd',
                MarketingData::CHANNEL_ORGANIC,
                'google'
            ],
            [
                'http://gorapid.com.au',
                'http://www.bing.com.au/something/asdasd',
                MarketingData::CHANNEL_ORGANIC,
                'bing'
            ],
            [
                'http://gorapid.com.au',
                'http://www.ask.com/',
                MarketingData::CHANNEL_ORGANIC,
                'ask'
            ],
            [
                'http://gorapid.com.au',
                'http://www.aol.com/',
                MarketingData::CHANNEL_ORGANIC,
                'aol'
            ],
            [
                'http://gorapid.com.au',
                'http://plus.google.com/123123123',
                MarketingData::CHANNEL_SOCIAL,
                'plus.google'
            ],
            [
                'http://gorapid.com.au',
                'http://www.m.facebook.com/profile?1123123',
                MarketingData::CHANNEL_SOCIAL,
                'facebook'
            ],
            [
                'http://gorapid.com.au',
                'http://www.facebook.com/profile?1123123',
                MarketingData::CHANNEL_SOCIAL,
                'facebook'
            ],
            [
                'http://gorapid.com.au',
                'http://www.instagram.com/profile?1123123',
                MarketingData::CHANNEL_SOCIAL,
                'instagram'
            ],
            [
                'http://gorapid.com.au',
                'http://www.facebook.com/google?1123123',
                MarketingData::CHANNEL_SOCIAL,
                'facebook'
            ],
            [
                'http://gorapid.com.au',
                'http://www.google.com/facebook?instagram=facebook',
                MarketingData::CHANNEL_ORGANIC,
                'google'
            ],
            [
                'http://gorapid.com.au/google/?instagram=facebook',
                null,
                MarketingData::CHANNEL_DIRECT,
                'direct'
            ],
            [
                'http://gorapid.com.au',
                'invalidurl',
                MarketingData::CHANNEL_DIRECT,
                'direct'
            ],
            [
                'http://gorapid.com.au',
                'www.invalidurl.com',
                MarketingData::CHANNEL_DIRECT,
                'direct'
            ],
            [
                'http://gorapid.com.au',
                null,
                MarketingData::CHANNEL_DIRECT,
                'direct'
            ],
        ];
    }

    /**
     * @dataProvider validUrlComboDataProvider
     */
    public function testValidUrlCombo($landingPage, $referer)
    {
        $this->marketizer = new Marketizer();
        $data             = $this->marketizer->getMarketingData($landingPage, $referer);

        $this->assertInstanceOf('Rapid\Marketizer\Data\MarketingData', $data);
    }

    public function validUrlComboDataProvider()
    {
        return [
            ['http://gorapid.com.au', null],
            ['http://www.gorapid.com.au', null],
            ['http://gorapid.com.au/finance/', null],
            ['http://gorapid.com.au/finance/?type=company', null],
            ['http://mobile.gorapid.com.au', null],
            ['http://gorapid.com.au', 'http://www.google.com'],
            ['http://gorapid.com.au', 'http://m.facebook.com'],
            ['http://gorapid.com.au', 'http://m.facebook.com/profile/123123'],
            ['http://gorapid.com.au', 'http://m.facebook.com/profile/123123?marketing=true'],
        ];
    }

    /**
     * @dataProvider invalidLandingPageDataProvider
     * @expectedException Rapid\Marketizer\Exception\UrlMalformedException
     * @expectedExceptionMessage Url provided is malformed
     */
    public function testInvalidLandingPage($landingPage)
    {
        $this->marketizer = new Marketizer();
        $this->marketizer->getMarketingData($landingPage, null);
    }

    public function invalidLandingPageDataProvider()
    {
        return [
            [''],
            ['ht'],
            ['http://'],
            ['http//gorapid.com.au'],
            ['#@%://www.gorapid.com.au'],
            ['gorapid.com.au/finance/'],
            ['http:/mobile.gorapid.com.au'],
        ];
    }

    /**
     * @dataProvider validCampaignDataProvider
     */
    public function testValidCampaignData($landingPage, $campaign)
    {
        $this->marketizer = new Marketizer();
        $data             = $this->marketizer->getMarketingData($landingPage, null);

        $this->assertInstanceOf('Rapid\Marketizer\Data\MarketingData', $data);
        $this->assertEquals($campaign, $data->getCampaign());
    }

    public function validCampaignDataProvider()
    {
        $campaignOptions = MarketingData::$campaignOptions;

        return [
            ['http://gorapid.com.au/?utm_campaign=facebook', 'facebook'],
            ['http://gorapid.com.au/?campaign=adwords', 'adwords'],
            ['https://gorapid.com.au/resources/customer-reviews/?campaign=RNA&adgroup=RNA&keyword=', 'RNA'],
            [
                'https://gorapid.com.au/bad-credit-loans/car-finance/' .
                '?utm_source=Facebook&utm_medium=Facebook+Ads&utm_term' .
                '=Bad+Credit+Car+Loans+-+&utm_content=Bad+Credit+Car+' .
                'Loans+-+&utm_campaign=Bad+Credit+Car+Loans+-&green+hsv' .
                '+red+stripe', 'Bad Credit Car Loans -'],
        ];
    }

    /**
     * @dataProvider invalidCampaignDataProvider
     */
    public function testInvalidCampaignData($landingPage)
    {
        $this->marketizer = new Marketizer();
        $data             = $this->marketizer->getMarketingData($landingPage, null);

        $this->assertInstanceOf('Rapid\Marketizer\Data\MarketingData', $data);
        $this->assertEquals(null, $data->getCampaign());
    }

    public function invalidCampaignDataProvider()
    {
        return [
            ['https://gorapid.com.au/resources/customer-reviews/?adgroup=RNA&keyword='],
            ['https://gorapid.com.au/bad-credit-loans/car-finance/?utm_medium=Facebook'],
            ['https://gorapid.com.au/bad-credit-loans/car-finance/?utm_term=Bad+Credit+Car+Loans+-'],
            ['https://gorapid.com.au/bad-credit-loans/car-finance/?utm_content=Bad+Credit+Car+Loans+-+'],
            ['http://gorapid.com.au/?ustm_campaign=facebook'],
            ['http://gorapid.com.au/?campaigns=adwords'],
            ['http://gorapid.com.au/?adwords=campaign'],
            ['http://gorapid.com.au/?google=utm_campaign'],
            ['http://utm-campaign.com.au'],
            ['http://gorapid.com.au/utm-campaign'],
            ['http://gorapid.com.au/?utm_campaign='],
        ];
    }
}
