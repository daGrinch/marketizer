# Marketizer for Rapid

This library provides easy structuring for data that is fed into the Rapid APIs.
Relavent Marketing information that is identified in URLs supplied by the user, will be
filtered out and structured into a common format that may be used to feed into
the Rapid Session endpoint.

# Installation

## Install via composer

Before installing, make sure that [Composer](https://getcomposer.org/) is installed. And add the following section
to access the private repository from Bitbucket.

	{
	    "repositories": [
	        {
	            "type": "vcs",
	            "url":  "git@bitbucket.org:gorapid/marketizer.git"
	        }
	    ]
	}

To install, include the following line in the [Composer](https://getcomposer.org/) require block.

	{
	    "require": {
	        "gorapid/marketizer": "dev-master"
	    }
	}

Or simply execute the following [Composer](https://getcomposer.org/) command.

	composer require "gorapid/marketizer=dev-master"

# Usage

Include the autoload file of the repository in the code.

```
	require __DIR__ . '/vendor/autoload.php';
```

Instantiate the Marketizer Class and test with the URLs.

```
use Rapid\Marketizer\Marketizer;

$marketizer = new Marketizer();

// feed landing page http://gorapid.com.au and referer http://www.facebook.com
$marketingData = $marketizer->getMarketingData('http://gorapid.com.au', 'http://www.facebook.com');

// optionally add ip, user agent and conversion page
$marketingData->setRemoteAddress('xxx.xxx.xxx.xxx');
$marketingData->setUserAgent('Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36');
$marketingData->setConversionPage('http://gorapid.com.au/finance/');

// convert result object to array
$marketingData->toArray();

// use the generated <MarketingData> $marketingData object as required
```